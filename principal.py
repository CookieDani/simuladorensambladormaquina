opcode = {"ADD": 0x83, "SUB": 0x83, "MOV":0xB8,
          "INT":0xCD,"DIV": 0xF7, "MUL":0xF7,  "JE": 0x74, "JMP": 0xEB, "CMP":0x83, "XOR":0x31}

registros = { "EAX": 0, "ECX": 1, "EDX": 2, "EBX": 3}
valoresRES = {}
valoresEQU = {}
valoresDB = {}
especialChar = {"0xA": "", "0xD": "\n"}
lnsASM = []


if __name__ == '__main__':
    with open("operaciones.asm", "r") as f:
        documento = f.readlines()
    print("------------------------------------------DOCUMENTO ORIGINAL-----------------------------------------")
    for linea in documento:
        print(linea, end="") 

    print("\n\n----------------------------------------ELIMINAR COMENTARIOS----------------------------------------")
    for i in range(len(documento)):
        linea = documento[i]
        ubicacionPuntoYComa = linea.find(";")
        if ubicacionPuntoYComa >= 0: # Genera un -1 si no encuentra
            linea = linea[:ubicacionPuntoYComa]
        linea = linea.replace(",", " ").replace("\n", "").replace("\t", "")

        linea = linea.split(" ") # remplaza el caracter "," por " " y posteriormente los divides cada instrucción delimitada por " "
        print(linea)
        documento[i] = linea


    lineasTrabajadas = [] # lista auxiliar para anotar los indices de las lineas que ya se han trabajado para poder eliminarlas
    for i in range(len(documento)):
        if len(documento[i]) > 1:
            if documento[i][1].upper() == "EQU" or documento[i][1].upper() == "DB":
                formado = ""
                bandera = True
                for j in range(2, len(documento[i])):
                    if documento[i][j] == "":
                        continue
                    if documento[i][j].find("$-") is not -1:
                        #print(documento[i][0], " ===== " , hex(len(valores.get(documento[i][j+1]))))
                        valoresDB[documento[i][0]] = hex(len(valoresDB.get(documento[i][j + 1])))
                        bandera = False
                        break
                    elif documento[i][j] in especialChar:
                        formado += especialChar.get(documento[i][j])
                    else:
                        formado += documento[i][j].replace('"','',2) + " "
                if bandera:
                    if documento[i][1].upper() == "EQU":
                        valoresEQU[documento[i][0]] = formado
                    else:
                        valoresDB[documento[i][0]] = formado
                lineasTrabajadas.append(i)
    lineasTrabajadas.reverse()
    for i in lineasTrabajadas:
        documento.pop(i)
    print("\n\n----------------------------------------------------VALORES EQU----------------------------------------------------")
    for k,v in valoresEQU.items():
        print(k, " =\n", v)
    print("----------------------------------------------------VALORES DB----------------------------------------------------")
    for k,v in valoresDB.items():
        print(k, " =\n", v)

    print(
        "----------------------------------------------------VALORES BSS----------------------------------------------------")
    memoria = 100
    lineasTrabajadas = []
    resb = []
    for i in range(len(documento)):
        if len(documento[i])>2:
            if documento[i][1].upper() == "RESB":
                lineasTrabajadas.append(i)
                valoresRES[documento[i][0]] = bin(memoria)[2:].zfill(8)
                resb.append(documento[i][0])
                memoria += int(documento[i][2])

    for k, v in valoresRES.items():
        print(k, " =\n", v)
    lineasTrabajadas.reverse()
    for i in lineasTrabajadas:
        documento.pop(i)
    print("\n\n-------------------------------------------------PROCEDIMIENTO-------------------------------------------------")
    cuentaVariables = 0
    for i in range(len(documento)):
        codigoHex = ""
        if documento[i][0] == '':
            continue
        print("***")
        bandera = False
        if documento[i][0].upper() in opcode:
            print("Instrucción: ", " ".join(documento[i]) , documento[i])
            print("Se esta trabajando con el opcode: ", documento[i][0], " en hexadecimal equivale a ", hex(opcode.get(documento[i][0].upper()))[2:].upper())
            bandera = True
        if documento[i][0].upper() == "MOV":
            print("OPERACION = ", "MOVER")
            if documento[i][1].upper() in registros:
                print("El segundo valor de la instrucción es un registro con valor", registros.get(documento[i][1].upper()))
                codigoHex += str(hex(opcode.get("MOV") + registros.get(documento[i][1].upper()))[2:]).upper()

                if documento[i][3] in valoresEQU:
                    print("El tercer valor de la instrucción es una constante guardada con valor: " + valoresEQU.get(documento[i][3]))
                    codigoHex += "{:02d}".format(int(valoresEQU.get(documento[i][3])))
                    codigoHex += "0"*6
                elif documento[i][3].isnumeric():
                    print("El tercer valor de la instrucción es un valor inmediato igual a: ", documento[i][3])
                    codigoHex += "{:02d}".format(int(documento[i][3]))
                    codigoHex += "0" * 6
                elif documento[i][3] in valoresDB:
                    if valoresDB.get(documento[i][3])[:2] == "0x":
                        print("El tercer valor de la instrucción es una variable numerica con valor:", "{:02x}".format(int(valoresDB.get(documento[i][3]), 16)).upper())
                        codigoHex += "{:02x}".format(int(valoresDB.get(documento[i][3]), 16)).upper()
                        codigoHex += "0" * 6
                    else:
                        print("El tercer valor sera guardado al final del documento se necesita calcular su posicion al final")
                        cuentaVariables+=1
                        codigoHex += "[-------]"
                elif documento[i][3].replace("[","").replace("]","") in valoresRES:
                    print("El tercer valor esta en memoria se coloca su posicion")
                    codigoHex+= valoresRES.get(documento[i][3].replace("[","").replace("]",""))
                else:
                    codigoHex += "[-------]"
            else:
                print("El segundo valor de la instrucción accede a memoria:")
                documento[i][1] = documento[i][1].replace("[","")
                documento[i][1] =documento[i][1].replace("]", "")

                if documento[i][3].upper() == "EAX":
                    codigoHex = str(0xA1+(int(resb.index(documento[i][1]))+1))[2:].zfill(2)+ valoresRES.get(documento[i][1])

            lnsASM.append(documento[i])
            documento[i] = codigoHex
        elif documento[i][0].upper() == "INT":
            print("OPERACION = INTERRUMPIR")
            print("Se añade el opcode y el valor inmediato de la interrupcion")
            codigoHex += str(hex(opcode.get("INT")))[2:].upper() + documento[i][1][2:]
            lnsASM.append(documento[i])
            documento[i] = codigoHex
        elif documento[i][0].upper() == "ADD":
            print("OPERACION = SUMAR")
            print("Se realiza un cálculo de dirección efectiva")
            if documento[i][1].upper() == "EAX":
                if documento[i][3] == "'0'":
                    codigoHex += str(hex(opcode.get("ADD")))[2:].upper() + "C030"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
                elif documento[i][3].upper() == "EBX":
                    codigoHex += "01D8"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
            if documento[i][1].upper() == "EBX":
                if documento[i][3] == "'0'":
                    codigoHex += str(hex(opcode.get("SUB")))[2:].upper() + "EB30"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
        elif documento[i][0].upper() == "SUB":
            print("OPERACION = RESTAR")
            print("Se realiza un cálculo de dirección efectiva")
            if documento[i][1].upper() == "EAX":
                if documento[i][3] == "'0'":
                    codigoHex += str(hex(opcode.get("SUB")))[2:].upper() + "E830"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
                elif documento[i][3].upper() == "EBX":
                    codigoHex += "29D8"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
            if documento[i][1].upper() == "EBX":
                if documento[i][3] == "'0'":
                    codigoHex += str(hex(opcode.get("SUB")))[2:].upper() + "EB30"
                    lnsASM.append(documento[i])
                    documento[i] = codigoHex
        elif documento[i][0].upper() == "MUL":
            print("OPERACION = MULTIPLICAR")
            print("Se realiza un cálculo de dirección efectiva")
            codigoHex += str(hex(opcode.get("MUL")))[2:].upper()
            if documento[i][1].upper() == "EBX":
                codigoHex += "E3"
            lnsASM.append(documento[i])
            documento[i] = codigoHex
        elif documento[i][0].upper() == "DIV":
            print("OPERACION = DIVIDIR")
            codigoHex += str(hex(opcode.get("DIV")))[2:].upper()
            print("Se realiza un cálculo de dirección efectiva")
            if documento[i][1].upper() == "ECX":
                codigoHex += "F1"
            lnsASM.append(documento[i])
            documento[i] = codigoHex
        elif documento[i][0].upper() == "XOR":
            print("OPERACION = LOGICO XOR")
            codigoHex += str(hex(opcode.get("XOR")))[2:].upper()
            if documento[i][1].upper() == "EDX" and documento[i][2].upper() == "EDX":
                codigoHex += "D2"
            elif documento[i][1].upper() == "EBX" and documento[i][3].upper() == "EBX":
                codigoHex += "DB"
            lnsASM.append(documento[i])
            documento[i] = codigoHex
        elif documento[i][0].upper() == "CMP":
            print("OPERACION = COMPARAR")
            if documento[i][1].upper() == "ECX":
                codigoHex += str(hex(opcode.get("CMP")))[2:].upper() + "F900"
                lnsASM.append(documento[i])
                documento[i] = codigoHex
        elif documento[i][0].upper() == "JE":
            print("OPERACION = SALTAR SI COMPARACION ANTERIOR DIO 0 (IGUALES)")
            lnsASM.append(documento[i])
            documento[i] = str(hex(opcode.get("JE")))[2:].upper() + "7438"
        elif documento[i][0].upper() == "JMP":
            print("OPERACION = SALTAR")
            lnsASM.append(documento[i])
            documento[i] = str(hex(opcode.get("JMP")))[2:].upper() + "16"
        if bandera:
            print("El valor que se generó es: ", documento[i])
        else:
            print("la línea en el documento es: ", documento[i])
        print("***")
    print("\n\n-------------------------------------------------RESULTADO-------------------------------------------------")
    cont = 0
    saliente = ""
    print("NUMERO DE INSTRUCCIONES: ",len(lnsASM)- cuentaVariables)
    nIntrucciones = len(lnsASM)- cuentaVariables
    for linea in documento:
        if isinstance(linea, str):
            if linea == '':
                continue
            if linea[2] == "[":
                nIntrucciones+=1
                linea = linea[:2]+hex(nIntrucciones)[2:].zfill(8)

            print("{0:20}  ->  {1:10}".format(" ".join(lnsASM[cont]), linea), " ->  ", bin(int(linea, 16))[2:])
            saliente+=linea + "\n"
            cont += 1
        else:
            continue

    for v in valoresDB.values():
        if len(v) <= 4:
            continue

        print("{0:35}  ->  ".format(v), end = "")
        for letter in v:
            print(hex(ord(letter))[2:].upper() ,end="")
            saliente+= hex(ord(letter))[2:].upper()
        saliente += "\n"


    with open("salida.com", "w") as f2:
        f2.write(saliente)
